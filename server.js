'use strict';

let express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);

let users = [],
    connections = [];

server.listen(process.env.PORT || 3000);
console.log('Run server');

app.use(express.static(__dirname + '/public/'));

app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/index.html`);
});

io.sockets.on('connection', socket => {
    connections.push(socket);
    console.log(`Connected: ${connections.length} sockets connected`);

    // Disconnect
    socket.on('disconnect', data => {
        users.splice(users.indexOf(socket.username), 1);
        updateUsernames();
        connections.splice(connections.indexOf(socket), 1);
        console.log(`Disconnected: ${connections.length} sockets connected`);
    });

    // Send message
    socket.on('send message', data => {
        io.sockets.emit('new message', {
            msg: data, 
            user: socket.username
        });
        console.log(`User(${socket.username}): ${data}`);
    });

    // New user
    socket.on('new user', (data, callback) => {
        if (~users.indexOf(data.toLowerCase())) {
            callback(false);
        } else {
            callback(true);
            socket.username = data;
            users.push(socket.username);
            updateUsernames();
        }
    });

    function updateUsernames() {
        io.sockets.emit('get users', users);
    }
})