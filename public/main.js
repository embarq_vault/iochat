'use strict';

$(function () {
    let socket = io.connect(),
        $nano = $('.nano'),
        $chat = $('#chat'),
        $users = $('#users'),

        $messageFormArea = $('#messageFormArea'),
        $messageForm = $('#messageForm'),
        $message = $('#message'),

        $userFormArea = $('#userFormArea'),
        $userForm = $('#userForm'),
        $username = $('#username');

    $nano.nanoScroller();

    $messageForm.submit(e => {
        e.preventDefault();
        socket.emit('send message', $message.val());
        $('.nano').nanoScroller({scroll: 'bottom'});
        $message.val('');
    });

    $userForm.submit(e => {
        e.preventDefault();
        socket.emit('new user', $username.val(), data => {
            if (data) {
                $userFormArea.hide();
                $messageFormArea.show();
            } else {
                alert('The username is already taken! Please choose another one');
            }
            $username.val('');
        })
    })

    socket.on('new message', data => {
        const id = `${data.user}-${Math.random() * 10}`;
        const chatEntryElement = `<div class="well" id=${id}><strong>${data.user}</strong>: ${data.msg}</div>`;
        $chat.append(chatEntryElement);
    });

    socket.on('get users', data =>
        $users.html(data.map(
            username => `<li class="list-group-item">${username}</li>`
        ).join('')));
});